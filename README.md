# Amendmend Editor

This is an example of a user interface, interacting with the legal-consolidator library. More information about requirements of the input data can be found [here](https://www.overleaf.com/read/ttnvxdtbjsnr).

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)

## Running on linux

- Make sure [Rust](https://www.rust-lang.org/tools/install) is installed
- Run `npm install`
- Run `npm run tauri dev` for preview
- Run `npm run tauri build` to compile `.deb` and `.AppImage` files

## Runnin on Windows

Same steps as for Linux.

Unfortunately the pdf parser currently will not work because the interaction with the python binary does not seem to work properly on windows. Maybe compiling the python binary from `pdf_parse.py` via a different method will bring better results.
