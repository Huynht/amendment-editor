import fitz
import sys

output = ""

filepath = input()

doc = fitz.open(filepath)

for pagenumber in range(doc.page_count):
    page = doc[pagenumber]
    blocks = page.get_text("blocks")
    for block in blocks:
        block_text = block[4]
        output += block_text + "\n"

print(output)
sys.stdout.flush()
