import { Route, Routes } from 'react-router-dom'
import Header from './components/Header'
import Consolidate from './views/Consolidate'
import Test from './views/Test'

export default function App() {
  return (
    <>
      <div className="flex h-full w-full flex-col">
        <Header />
        <div className="h-[calc(100vh-34px)]">
          <Routes>
            <Route path="/" element={<Consolidate />} />
            <Route path="/consolidate" element={<Consolidate />} />
            <Route path="/test" element={<Test />} />
          </Routes>
        </div>
      </div>
    </>
  )
}
