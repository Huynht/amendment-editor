import { writeText } from '@tauri-apps/api/clipboard'
import { FileEntry } from '@tauri-apps/api/fs'
import { Command } from '@tauri-apps/api/shell'
import { useEffect, useState } from 'react'
import { buttonStyle, outlineButtonStyle } from '../styles'
import PdfViewer from './PdfViewer'

type Props = { selectedFile: FileEntry | undefined }

function AmendingDocument(props: Props) {
  const [textBlocks, setTextBlocks] = useState<string[]>([])

  const [selectedTextBlocks, setSelectedTextBlocks] = useState<
    { text: string; index: number }[]
  >([])

  const [rerenderCounter, setRerenderCounter] = useState(0)

  const [showParsed, setShowParsed] = useState(false)

  const parsePdfCommand = Command.sidecar('pythonScripts/parsePdf')
  parsePdfCommand.stdout.on('data', (data) => {
    setTextBlocks((prev) => {
      prev.push(data)
      return prev
    })
  })

  function toggleCheckbox(i: number) {
    const checkboxElement = document.getElementById(
      `checkbox-amending-doc-${i}`
    )

    if (checkboxElement) {
      checkboxElement.click()
    } else {
      console.error(`Checkbox for index ${i} not found`)
    }
  }

  function clickTextBoxContainer(i: number) {
    const checkboxElement = document.getElementById(`text-block-container-${i}`)

    if (checkboxElement) {
      checkboxElement.click()
    } else {
      console.error(`Checkbox for index ${i} not found`)
    }
  }

  function getPdfButtonStyle() {
    if (showParsed) {
      return outlineButtonStyle + ' rounded-r-none'
    }

    return buttonStyle + ' hover:bg-blue-300 rounded-r-none'
  }

  function getParsedButtonStyle() {
    if (!textBlocks || textBlocks.length === 0) {
      return (
        outlineButtonStyle +
        ' cursor-default hover:bg-white text-gray-500 rounded-l-none'
      )
    }

    if (!showParsed) {
      return outlineButtonStyle + ' rounded-l-none'
    }

    return buttonStyle + ' hover:bg-blue-300 rounded-l-none'
  }

  function keyDownEvent(event: React.KeyboardEvent<HTMLDivElement>) {
    if (event.code === 'KeyC' && event.ctrlKey === true) {
      writeSelectionToClipboard()
    }
  }

  function writeSelectionToClipboard() {
    let text = ''
    selectedTextBlocks
      .sort((a, b) => a.index - b.index)
      .forEach((block) => {
        text += block.text
        toggleCheckbox(block.index)
      })

    text = text.trim()
    if (text.startsWith('"')) {
      text = text.slice(1)
    }
    if (text.endsWith('"')) {
      text = text.slice(0, -1)
    }
    text.replaceAll('"', '\\"')

    writeText(text)

    setSelectedTextBlocks([])
  }

  return (
    <>
      <div
        className="flex h-full flex-col gap-2"
        onKeyDown={keyDownEvent}
        tabIndex={0}
      >
        {props.selectedFile && (
          <div className="flex gap-2">
            <div className={buttonStyle} onClick={writeSelectionToClipboard}>
              Copy selection to clipboard
            </div>
            <div
              className={buttonStyle + ' ml-auto'}
              onClick={() => {
                setTextBlocks([])
                parsePdfCommand.spawn().then((child) => {
                  child.write(props.selectedFile!.path + '\n')
                  setTimeout(() => {
                    setRerenderCounter(rerenderCounter + 1)
                  }, 2000)
                })
              }}
            >
              Parse PDF
            </div>
            <div className="flex">
              <div
                className={getPdfButtonStyle()}
                onClick={() => {
                  setShowParsed(false)
                }}
              >
                Pdf
              </div>
              <div
                className={getParsedButtonStyle()}
                onClick={() => {
                  if (textBlocks && textBlocks.length > 0) {
                    setShowParsed(true)
                  }
                }}
              >
                Parsed
              </div>
            </div>
          </div>
        )}
        {props.selectedFile && (
          <div
            className={`h-full overflow-auto rounded-md border-2 p-2 ${
              showParsed && 'hidden'
            }`}
          >
            <PdfViewer file={props.selectedFile} />
          </div>
        )}
        {textBlocks && textBlocks.length > 0 && (
          <div
            className={`h-full overflow-auto rounded-md border-2 p-2 ${
              !showParsed && 'hidden'
            }`}
          >
            {textBlocks.map((block, i) => {
              return (
                <div
                  id={`text-block-container-${i}`}
                  key={i}
                  onClick={() => {
                    toggleCheckbox(i)

                    const j = i + 1
                    if (
                      textBlocks[j] &&
                      textBlocks[j].replace(' ', '') !== ''
                    ) {
                      clickTextBoxContainer(j)
                    }
                  }}
                  className="flex cursor-pointer gap-2 hover:bg-slate-200"
                >
                  <div>{block}</div>
                  <div className="ml-auto mr-4">
                    <input
                      type="checkbox"
                      id={`checkbox-amending-doc-${i}`}
                      onClick={(e) => {
                        e.stopPropagation()
                      }}
                      onChange={(e) => {
                        if (e.currentTarget.checked) {
                          setSelectedTextBlocks((prev) => {
                            prev.push({ text: block, index: i })
                            return prev
                          })
                        } else {
                          setSelectedTextBlocks((prev) => {
                            const newList = prev.filter(
                              (item) => item.text !== block
                            )
                            return newList
                          })
                        }
                      }}
                    />
                  </div>
                </div>
              )
            })}
          </div>
        )}
      </div>
    </>
  )
}

export default AmendingDocument
