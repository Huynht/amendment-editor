import {
  ArrowDownTrayIcon,
  ArrowPathIcon,
  ArrowsRightLeftIcon,
  CodeBracketIcon,
  DocumentMinusIcon,
  DocumentPlusIcon,
  MagnifyingGlassIcon,
  PaintBrushIcon,
  TrashIcon,
} from '@heroicons/react/24/outline'
import { save } from '@tauri-apps/api/dialog'
import { FileEntry, writeTextFile } from '@tauri-apps/api/fs'
import { useAtomValue } from 'jotai'
import {
  AmendingInstruction,
  ChapterKey,
  ReplaceKeywordInstruction,
  ReplacePartlyInstruction,
} from 'legal-consolidator'
import _ from 'lodash'
import type monaco from 'monaco-editor'
import { useEffect, useMemo, useRef, useState } from 'react'
import { currentInputAtom } from '../store'
import { buttonStyle } from '../styles'
import { getInstructionIndexFromLineNumber } from '../utils/editorUtils'
import { instructionsToJson } from '../utils/jsonUtils'
import { scrollToChapter } from '../utils/scroll'
import EditorWrapper from './EditorWrapper'

type Props = {
  selectedFile: FileEntry | undefined
  selectedAmendingDoc: FileEntry | undefined
  onApply: (instructions: AmendingInstruction[]) => void
  onWriteFile: () => void
}

export default function AmendmentInput(props: Props) {
  const currentInputState = useAtomValue(currentInputAtom)

  const editor = useRef<monaco.editor.IStandaloneCodeEditor | null>(null)

  const [fileToInputMap, setFileToInputMap] = useState<Map<FileEntry, string>>(
    new Map()
  )
  const [cursorLine, setCursorLine] = useState<number>()

  useEffect(() => {
    if (props.selectedFile) {
      if (!fileToInputMap.get(props.selectedFile)) {
        updateInputMap(props.selectedFile, `[\n\t\n]`)
      }
    }
  }, [props.selectedFile])

  const currentInput = useMemo(() => {
    if (props.selectedFile) {
      const currentInput = fileToInputMap.get(props.selectedFile)
      return currentInput
    }
  }, [props.selectedFile, fileToInputMap])

  useEffect(() => {
    if (currentInputState && props.selectedFile) {
      const mapCopy = _.cloneDeep(fileToInputMap)
      mapCopy.set(props.selectedFile, currentInputState)

      setFileToInputMap(mapCopy)
    }
  }, [currentInputState])

  const currentInstructions: AmendingInstruction[] | undefined = useMemo(() => {
    if (!currentInput || currentInput === '') {
      return undefined
    }

    let instructions
    try {
      instructions = JSON.parse(currentInput)
    } catch {}

    if (instructions) return instructions
    return undefined
  }, [currentInput])

  // USER INPUT
  function scrollToPreview() {
    if (!currentInstructions || !cursorLine) {
      console.error('one of these is falsy: ', currentInstructions, cursorLine)
      return
    }

    const instructionIndex = getInstructionIndexFromLineNumber(
      currentInstructions,
      cursorLine
    )
    if (instructionIndex === undefined) {
      console.error('couldnt get instruction index from linenumber')
      return
    }

    const instruction = currentInstructions[instructionIndex]

    let targetChapterKey
    if (instruction.type === 'RENUMBER_MULTIPLE') {
      targetChapterKey = instruction.newKeys.from
    } else if (instruction.type === 'RENUMBER') {
      targetChapterKey = instruction.newChapterKey
    } else {
      targetChapterKey = instruction.targetChapterKey
    }

    scrollToChapter(targetChapterKey)
  }

  function formatCode() {
    if (!editor.current) {
      console.error('editor ref not found')
      return
    }

    const formatAction = editor.current.getAction(
      'editor.action.formatDocument'
    )
    if (!formatAction) {
      console.error('no action found')
      return
    }

    formatAction.run()
  }

  function deleteInstruction() {
    if (!currentInstructions || !cursorLine) {
      console.error('one of these is falsy: ', currentInstructions, cursorLine)
      return
    }

    const instructionIndex = getInstructionIndexFromLineNumber(
      currentInstructions,
      cursorLine
    )
    if (instructionIndex === undefined) {
      console.error('couldnt get instruction index from linenumber')
      return
    }

    const newInstructions = _.cloneDeep(
      currentInstructions
        .slice(0, instructionIndex)
        .concat(currentInstructions.slice(instructionIndex + 1))
    )
    const newInputJson =
      newInstructions.length === 0
        ? '[\n\t\n]'
        : instructionsToJson(newInstructions)

    updateInputMap(props.selectedFile!, newInputJson)

    if (newInstructions.length !== 0) {
      formatCode()
    }
  }

  function addDelete(currentFile: FileEntry) {
    let instructionsCopy: AmendingInstruction[]
    if (!currentInstructions) {
      instructionsCopy = []
    } else {
      instructionsCopy = _.cloneDeep(currentInstructions)
    }

    instructionsCopy.push({
      type: 'DELETE',
      targetChapterKey: new ChapterKey('INSERT_CHAPTER_KEY'),
    })

    updateInputMap(currentFile, instructionsToJson(instructionsCopy))

    formatCode()
  }

  function addInsert(currentFile: FileEntry) {
    let instructionsCopy: AmendingInstruction[]
    if (!currentInstructions) {
      instructionsCopy = []
    } else {
      instructionsCopy = _.cloneDeep(currentInstructions)
    }

    instructionsCopy.push({
      type: 'INSERT',
      targetChapterKey: new ChapterKey('INSERT_CHAPTER_KEY'),
      newContent: 'NEW_CONTENT_GOES_HERE',
    })

    updateInputMap(currentFile, instructionsToJson(instructionsCopy))

    formatCode()
  }

  function addRenumber(currentFile: FileEntry) {
    let instructionsCopy: AmendingInstruction[]
    if (!currentInstructions) {
      instructionsCopy = []
    } else {
      instructionsCopy = _.cloneDeep(currentInstructions)
    }

    instructionsCopy.push({
      type: 'RENUMBER',
      targetChapterKey: new ChapterKey('INSERT_TARGET_CHAPTER_KEY'),
      newChapterKey: new ChapterKey('INSERT_NEW_CHAPTER_KEY'),
    })

    updateInputMap(currentFile, instructionsToJson(instructionsCopy))

    formatCode()
  }

  function addRenumberMultiple(currentFile: FileEntry) {
    let instructionsCopy: AmendingInstruction[]
    if (!currentInstructions) {
      instructionsCopy = []
    } else {
      instructionsCopy = _.cloneDeep(currentInstructions)
    }

    instructionsCopy.push({
      type: 'RENUMBER_MULTIPLE',
      formerKeys: {
        from: new ChapterKey('FORMER_FROM'),
        to: new ChapterKey('FORMER_TO'),
      },
      newKeys: {
        from: new ChapterKey('NEW_FROM'),
        to: new ChapterKey('NEW_TO'),
      },
    })

    updateInputMap(currentFile, instructionsToJson(instructionsCopy))

    formatCode()
  }

  function addReplace(currentFile: FileEntry) {
    let instructionsCopy: AmendingInstruction[]
    if (!currentInstructions) {
      instructionsCopy = []
    } else {
      instructionsCopy = _.cloneDeep(currentInstructions)
    }

    instructionsCopy.push({
      type: 'REPLACE',
      targetChapterKey: new ChapterKey('INSERT_TARGET_CHAPTER_KEY'),
      newContent: 'NEW_CONTENT_HERE',
    })

    updateInputMap(currentFile, instructionsToJson(instructionsCopy))

    formatCode()
  }

  function addReplacePartly(currentFile: FileEntry) {
    let instructionsCopy: AmendingInstruction[]
    if (!currentInstructions) {
      instructionsCopy = []
    } else {
      instructionsCopy = _.cloneDeep(currentInstructions)
    }

    instructionsCopy.push({
      type: 'REPLACE_PARTLY',
      targetChapterKey: new ChapterKey('INSERT_TARGET_CHAPTER_KEY'),
      newContent: ['TEXT_BEFORE_ELLIPSIS', '...', 'TEXT_AFTER_ELLIPSIS'],
    } as ReplacePartlyInstruction)

    updateInputMap(currentFile, instructionsToJson(instructionsCopy))

    formatCode()
  }

  function addReplaceKeyword(currentFile: FileEntry) {
    let instructionsCopy: AmendingInstruction[]
    if (!currentInstructions) {
      instructionsCopy = []
    } else {
      instructionsCopy = _.cloneDeep(currentInstructions)
    }

    instructionsCopy.push({
      type: 'REPLACE_KEYWORD',
      targetChapterKey: new ChapterKey('INSERT_TARGET_CHAPTER_KEY'),
      keyword: 'KEYWORD_TO_BE_REPLACED',
      replacement: 'REPLACING_WORD',
      recursive: false,
    } as ReplaceKeywordInstruction)

    updateInputMap(currentFile, instructionsToJson(instructionsCopy))

    formatCode()
  }

  // UTIL
  function updateInputMap(key: FileEntry, value: string) {
    const mapCopy = _.cloneDeep(fileToInputMap)
    mapCopy.set(key, value)
    setFileToInputMap(mapCopy)
  }

  return (
    <>
      {props.selectedFile && (
        <div>
          <div className="flex flex-col gap-2">
            <div className="flex gap-2">
              <button
                className={buttonStyle + ' flex gap-2'}
                onClick={() => {
                  if (!currentInput || currentInput === '') {
                    return
                  }

                  try {
                    const instructions = JSON.parse(currentInput)

                    props.onApply(instructions)
                  } catch {
                    console.error('could not parse input correctly')
                  }
                }}
              >
                <PaintBrushIcon className="h-4 w-4" />
                Apply
              </button>
              <button
                className={buttonStyle + ' flex gap-2'}
                onClick={formatCode}
              >
                <CodeBracketIcon className="h-4 w-4" />
                Format
              </button>
              <button
                id="preview-button"
                className={buttonStyle + ' flex gap-2'}
                onClick={() => {
                  scrollToPreview()
                }}
              >
                <MagnifyingGlassIcon className="h-4 w-4" />
                View in document
              </button>
              <button
                className={buttonStyle + ' flex gap-2'}
                onClick={deleteInstruction}
              >
                <TrashIcon className="h-4 w-4" />
                Delete instruction
              </button>
              {currentInput && (
                <button
                  className={buttonStyle + ' ml-auto flex gap-2'}
                  onClick={async () => {
                    let defaultPath: string | undefined
                    if (!props.selectedAmendingDoc) {
                      defaultPath = props.selectedFile?.path.replaceAll(
                        '.html',
                        '.json'
                      )
                    }

                    if (props.selectedAmendingDoc) {
                      const identifier = props.selectedAmendingDoc.path
                        .split('/')
                        .slice(-4, -1)
                        .join('/')
                      defaultPath =
                        props.selectedAmendingDoc.path
                          .split('/')
                          .slice(0, -1)
                          .join('/') +
                        `/${identifier.replaceAll('/', '_')}.json`
                    }
                    const path = await save({
                      defaultPath: defaultPath,
                    })

                    if (path) {
                      await writeTextFile(path, currentInput)
                      props.onWriteFile()
                    }
                  }}
                >
                  <ArrowDownTrayIcon className="h-4 w-4" />
                  Save
                </button>
              )}
            </div>
            <div className="flex gap-2">
              <button
                className={buttonStyle + ' flex gap-2 text-[10px]'}
                onClick={() => addDelete(props.selectedFile!)}
              >
                <DocumentMinusIcon className="h-4 w-4" />
                DELETE
              </button>
              <button
                className={buttonStyle + ' flex gap-2 text-[10px]'}
                onClick={() => addInsert(props.selectedFile!)}
              >
                <DocumentPlusIcon className="h-4 w-4" />
                INSERT
              </button>
              <button
                className={buttonStyle + ' flex gap-2 text-[10px]'}
                onClick={() => addRenumber(props.selectedFile!)}
              >
                <ArrowsRightLeftIcon className="h-4 w-4 rotate-90" />
                RENUMBER
              </button>
              <button
                className={buttonStyle + ' flex gap-2 text-[10px]'}
                onClick={() => addRenumberMultiple(props.selectedFile!)}
              >
                <div className="flex">
                  <ArrowsRightLeftIcon className="h-4 w-4 rotate-90" />*
                </div>
                RENUMBER_MULTIPLE
              </button>
              <button
                className={buttonStyle + ' flex gap-2 text-[10px]'}
                onClick={() => addReplace(props.selectedFile!)}
              >
                <ArrowPathIcon className="h-4 w-4 rotate-90" />
                REPLACE
              </button>
              <button
                className={buttonStyle + ' flex gap-2 text-[10px]'}
                onClick={() => addReplacePartly(props.selectedFile!)}
              >
                <div className="flex">
                  <ArrowPathIcon className="h-4 w-4 rotate-90" />*
                </div>
                REPLACE_PARTLY
              </button>
              <button
                className={buttonStyle + ' flex gap-2 text-[10px]'}
                onClick={() => addReplaceKeyword(props.selectedFile!)}
              >
                <div className="flex">
                  <ArrowPathIcon className="h-4 w-4 rotate-90" />*
                </div>
                REPLACE_KEYWORD
              </button>
            </div>
          </div>

          <EditorWrapper
            key={props.selectedFile.path}
            onMount={(editorRef) => {
              editor.current = editorRef
            }}
            value={currentInput}
            onChange={(newValue) => {
              updateInputMap(props.selectedFile!, newValue)
            }}
            onLineChange={(lineNumber) => {
              setCursorLine(lineNumber)
            }}
          />
        </div>
      )}
    </>
  )
}
