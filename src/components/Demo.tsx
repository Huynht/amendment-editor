import { invoke } from '@tauri-apps/api'
import { appDataDir } from '@tauri-apps/api/path'
import { useState } from 'react'

export default function Demo() {
  const [greetMsg, setGreetMsg] = useState('')
  const [name, setName] = useState('')

  async function greetRs() {
    // Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
    setGreetMsg(await invoke('greet', { name }))
  }

  async function greetJs() {
    setGreetMsg(`Hello ${name}! You are greeted by Javascript.`)
  }

  return (
    <>
      <h1>Welcome to Tauri!</h1>

      <div className="row">
        <a href="https://tauri.app" target="_blank">
          <img src="/tauri.svg" className="logo tauri" alt="Tauri logo" />
        </a>
      </div>

      <p>Click on the Tauri, Vite, and React logos to learn more.</p>

      <div className="row">
        <input
          id="greet-input"
          onChange={(e) => setName(e.currentTarget.value)}
          placeholder="Enter a name..."
        />
        <button
          className="font-bold"
          onClick={() => {
            if (name) {
              greetRs()
            }
          }}
        >
          Rusty Greet
        </button>
        <button
          onClick={() => {
            if (name) {
              greetJs()
            }
          }}
        >
          GreetJs
        </button>
      </div>
      <p>{greetMsg}</p>

      <div
        className="m-2 w-56 cursor-pointer rounded-md border-2 p-2"
        onClick={async () => {}}
      >
        click me!
      </div>
    </>
  )
}
