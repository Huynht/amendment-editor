import { ArrowPathIcon, TrashIcon } from '@heroicons/react/24/outline'
import { open } from '@tauri-apps/api/dialog'
import { FileEntry, removeFile } from '@tauri-apps/api/fs'
import { useEffect, useState } from 'react'
import { buttonStyle } from '../styles'
import FileViewer from './FileViewer/FileViewer'
import IconButton from './IconButton'

type Props = {
  rerenderCounter: number
  onBaseDocSelected: (htmlFile: FileEntry) => void
  onAmendingDocSelected: (pdfFile: FileEntry) => void
  onAmendingInstructionsSelected: (jsonFile: FileEntry) => void
}

export default function DocumentPicker(props: Props) {
  const [basePath, setBasePath] = useState(
    '/home/an/Dropbox/Studium/aktuell/MA/example_base_html'
  )
  const [rerenderCounter, setRerenderCounter] = useState(props.rerenderCounter)

  useEffect(() => {
    setRerenderCounter(props.rerenderCounter)
  }, [props.rerenderCounter])
  useEffect(() => {
    setBasePath(basePath)
  }, [props.rerenderCounter])

  const [selectedFile, setSelectedFile] = useState<FileEntry>()

  return (
    <div className="flex h-full flex-col">
      <div className="flex gap-2">
        <button
          className={buttonStyle}
          onClick={() => {
            open({
              directory: true,
              multiple: false,
            }).then((selected) => {
              setBasePath(selected as string)
            })
          }}
        >
          Select base folder
        </button>
        <div className={buttonStyle}>
          <IconButton
            icon={<ArrowPathIcon />}
            onClick={() => setRerenderCounter(rerenderCounter + 1)}
          />
        </div>
        <button
          className={buttonStyle + ` flex gap-2`}
          onClick={async () => {
            if (!selectedFile) {
              alert('no file selected')
              return
            }
            const confirmed = await confirm(
              `Are you sure you want to delete ${selectedFile.name}?`
            )
            if (confirmed) {
              removeFile(selectedFile.path)
              setSelectedFile(undefined)
              setRerenderCounter(rerenderCounter + 1)
              return
            }
          }}
        >
          <TrashIcon className="h-4 w-4" />
          <div>Delete File</div>
        </button>
      </div>
      <div className="flex h-full flex-col">
        <FileViewer
          rerenderCounter={rerenderCounter}
          basePath={basePath}
          onFileSelected={async (file) => {
            if (file.path.includes('.pdf')) {
              props.onAmendingDocSelected(file)
              return
            }

            if (file.path.includes('.html')) {
              props.onBaseDocSelected(file)
              return
            }

            if (file.path.includes('.json')) {
              const confirmed = await confirm(
                'Are you sure? This will overwrite current progress'
              )
              if (confirmed) {
                props.onAmendingInstructionsSelected(file)
                return
              }
              return
            }
          }}
          filterSuffixes={['.html', '.pdf', '.json']}
        />
      </div>
    </div>
  )
}
