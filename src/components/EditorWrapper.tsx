import Editor from '@monaco-editor/react'
import type monaco from 'monaco-editor'
import { useState } from 'react'
import { amendingInstructionsSchema } from '../utils/jsonUtils'

type Props = {
  value: string | undefined
  onChange: (value: string) => void
  onLineChange: (lineNumber: number) => void
  onMount: (editor: monaco.editor.IStandaloneCodeEditor) => void
}

export default function EditorWrapper(props: Props) {
  return (
    <div className="my-4">
      {props.value && (
        <Editor
          defaultLanguage="json"
          theme="vs-dark"
          height="calc(40vh)"
          value={props.value}
          onChange={(value) => {
            if (!value) return

            props.onChange(value)
          }}
          beforeMount={(monaco) => {
            monaco.languages.json.jsonDefaults.setDiagnosticsOptions({
              allowComments: true,
              trailingCommas: 'error',
              validate: true,
              schemaValidation: 'error',
              schemas: [amendingInstructionsSchema],
            })
          }}
          onMount={(editor) => {
            props.onMount(editor)

            editor.onDidChangeCursorPosition((e) => {
              props.onLineChange(e.position.lineNumber)

              const previewButton = document.getElementById('preview-button')
              if (previewButton) {
                setTimeout(() => previewButton.click(), 100)
              }
            })
          }}
        />
      )}
    </div>
  )
}
