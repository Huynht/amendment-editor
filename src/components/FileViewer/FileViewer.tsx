import {
  ArrowPathIcon,
  CameraIcon,
  DocumentIcon,
  FolderIcon,
  FolderOpenIcon,
  TrashIcon,
} from '@heroicons/react/24/outline'
import { FileEntry, readDir } from '@tauri-apps/api/fs'
import { platform } from '@tauri-apps/api/os'
import { useEffect, useMemo, useState } from 'react'
// @ts-ignore
import FileBrowser from 'react-keyed-file-browser'
import '../../../node_modules/react-keyed-file-browser/dist/react-keyed-file-browser.css'
import './FileViewer.css'

export default function FileViewer(props: {
  rerenderCounter: number
  basePath: string
  onFileSelected: (file: FileEntry) => void
  filterSuffixes?: string[]
}) {
  const [os, setOs] = useState('')
  useEffect(() => {
    platform().then((res) => setOs(res))
  })
  const [fileEntries, setFileEntries] = useState<FileEntry[]>([])
  useEffect(() => {
    readDir(props.basePath, { recursive: true }).then((res) => {
      const fileEntries = readAndFilterFileEntries(res)
      setFileEntries(fileEntries)
    })
  }, [props.basePath, props.rerenderCounter])

  function readAndFilterFileEntries(fileEntries: FileEntry[]) {
    let output: FileEntry[] = []

    fileEntries.forEach((fileEntry) => {
      if (fileEntry.children) {
        output = output.concat(readAndFilterFileEntries(fileEntry.children))
      } else if (
        !props.filterSuffixes ||
        props.filterSuffixes.some(
          (suffix) => fileEntry.name && fileEntry.name.includes(suffix)
        )
      ) {
        output.push(fileEntry)
      }
    })

    return output
  }

  const files = useMemo(() => {
    const output: { key: string }[] = []
    if (fileEntries && fileEntries.length > 0) {
      fileEntries.forEach((htmlFile) => {
        output.push({
          key: htmlFile.path
            .replaceAll(props.basePath, '')
            .replaceAll('\\', '/'),
        })
      })
    }
    return output
  }, [fileEntries])

  return (
    <>
      {fileEntries && (
        <FileBrowser
          files={files}
          icons={{
            File: <DocumentIcon className="h-4 w-4" />,
            Image: <CameraIcon className="h-4 w-4" />,
            Rename: <DocumentIcon className="h-4 w-4" />,
            Folder: <FolderIcon className="h-4 w-4" />,
            FolderOpen: <FolderOpenIcon className="h-4 w-4" />,
            Delete: <TrashIcon className="h-4 w-4" />,
            Loading: <ArrowPathIcon className="h-4 w-4" />,
          }}
          onSelectFile={(file: { key: string }) => {
            const key =
              os === 'win32' ? file.key.replaceAll('/', '\\') : file.key
            const fullPath = props.basePath + key

            const fileEntry = fileEntries.filter(
              (fileEntry) => fileEntry.path === fullPath
            )[0]
            props.onFileSelected(fileEntry)
          }}
        />
      )}
    </>
  )
}
