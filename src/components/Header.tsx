import { Link } from 'react-router-dom'
import { HomeIcon, PencilSquareIcon } from '@heroicons/react/24/outline'
import _ from 'lodash'

export default function Header() {
  return (
    <>
      <div className="flex w-full gap-2 bg-gray-300">
        <Link to="/" className="flex items-center">
          <HomeIcon className="h-5 w-5" />
        </Link>
        <Link to="/consolidate" className="flex items-center">
          <PencilSquareIcon className="h-5 w-5" />
        </Link>
        <Link to="/test">
          <div className="rounded-md p-1 hover:bg-gray-400 hover:text-white">
            Test
          </div>
        </Link>
      </div>
    </>
  )
}
