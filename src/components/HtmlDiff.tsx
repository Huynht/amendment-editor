import { Change, diffChars } from 'diff'
import diff from 'html-diff-ts'

export default function HtmlDiff(props: {
  firstHtml: string
  secondHtml: string
}) {
  const diffHtmlString = diff(props.firstHtml, props.secondHtml, {})

  return <div dangerouslySetInnerHTML={{ __html: diffHtmlString }} />
}
