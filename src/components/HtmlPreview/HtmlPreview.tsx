import { ArrowUpTrayIcon } from '@heroicons/react/24/outline'
import { save } from '@tauri-apps/api/dialog'
import { FileEntry, readTextFile, writeTextFile } from '@tauri-apps/api/fs'
import diff from 'html-diff-ts'
import { AmendingInstruction, ConsolidationEngine } from 'legal-consolidator'
import { useEffect, useMemo, useState } from 'react'
import { buttonStyle, outlineButtonStyle } from '../../styles'
import './HtmlPreview.css'

type Props = {
  consolidationEngine: ConsolidationEngine
  baseDoc: FileEntry
  amendingDoc: FileEntry | undefined
  instructions: AmendingInstruction[] | undefined
  onWriteFile: () => void
}

export default function HtmlPreview(props: Props) {
  const [showPreview, setShowPreview] = useState(true)

  const [currentDocKey, setCurrentDocKey] = useState<string>()
  useEffect(() => {
    if (props.baseDoc && props.baseDoc.name) {
      readTextFile(props.baseDoc.path).then((res) => {
        if (props.baseDoc?.name) {
          if (
            !props.consolidationEngine.baseDocuments.has(props.baseDoc.name)
          ) {
            props.consolidationEngine.declareBaseDocument(
              props.baseDoc.name,
              res
            )
          }

          setCurrentDocKey(props.baseDoc.name)
        }
      })
    }
  }, [props.baseDoc])

  const originalHtmlString = useMemo(() => {
    if (currentDocKey) {
      const baseDoc = props.consolidationEngine.baseDocuments.get(currentDocKey)
      if (baseDoc) {
        return baseDoc.html.documentElement.innerHTML
      }
    }
  }, [currentDocKey, props.consolidationEngine])

  const newHtmlString = useMemo(() => {
    if (
      currentDocKey &&
      props.instructions &&
      props.instructions.length > 0 &&
      originalHtmlString
    ) {
      return props.consolidationEngine.consolidate(
        currentDocKey,
        '',
        props.instructions
      ).html.documentElement.innerHTML
    }

    return undefined
  }, [props.instructions])

  const htmlDiff = useMemo(() => {
    if (originalHtmlString && newHtmlString) {
      return diff(originalHtmlString, newHtmlString)
    }
    return undefined
  }, [newHtmlString])

  return (
    <div className="flex h-full flex-col">
      <div className="flex items-center gap-2">
        <div className="p-2 text-xl font-bold">
          {currentDocKey?.replaceAll('.html', '').replaceAll('_', ' ')}
        </div>
        <div className="ml-auto flex">
          <div
            className={
              (showPreview
                ? buttonStyle + ' hover:bg-blue-300'
                : outlineButtonStyle + ' hover:italic') + ' rounded-r-none'
            }
            onClick={() => {
              setShowPreview(!showPreview)
            }}
          >
            Preview
          </div>
          <div
            className={
              (showPreview
                ? outlineButtonStyle + ' hover:italic'
                : buttonStyle + ' hover:bg-blue-300') + ' rounded-l-none'
            }
            onClick={() => {
              setShowPreview(!showPreview)
            }}
          >
            Original
          </div>
        </div>
        <div
          className={buttonStyle}
          onClick={async () => {
            let defaultPath: string | undefined
            if (!props.amendingDoc) {
              defaultPath = props.baseDoc.path
            }

            if (props.amendingDoc) {
              const identifier = props.amendingDoc.path
                .split('/')
                .slice(-4, -1)
                .join('/')
              defaultPath =
                props.amendingDoc.path.split('/').slice(0, -1).join('/') +
                `/${identifier.replaceAll('/', '_')}.html`
            }

            const path = await save({
              defaultPath: defaultPath,
            })
            console.log('path', path, newHtmlString?.slice(0, 10000))

            if (path && newHtmlString) {
              await writeTextFile(path, newHtmlString)
              props.onWriteFile()
            }
          }}
        >
          <div className="flex gap-2">
            <ArrowUpTrayIcon className="h-4 w-4" />
            Export New Version
          </div>
        </div>
      </div>
      {(!showPreview || !htmlDiff) && originalHtmlString && (
        <div
          dangerouslySetInnerHTML={{ __html: originalHtmlString }}
          id="scrollable-document-container"
          className="m-2 h-full overflow-auto rounded-md border-2 p-2"
        />
      )}
      {showPreview && htmlDiff && originalHtmlString && (
        <div
          dangerouslySetInnerHTML={{ __html: htmlDiff }}
          id="scrollable-document-container"
          className="m-2 h-full overflow-auto rounded-md border-2 p-2"
        />
      )}
    </div>
  )
}
