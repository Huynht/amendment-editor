import { MouseEvent, ReactNode, useState } from 'react'
import { buttonStyle, negativeButtonStyle } from '../styles'
import Modal from './Modal'

type Props = {
  icon: ReactNode
  onClick: (e: MouseEvent<any>) => void
  tooltip?: string
  disabled?: boolean
  confirmationText?: string
}

export default function IconButton(props: Props) {
  const {
    tooltip = 'This button does not have a tooltip yet. Contact the devs if you want to have a better explanation of this button added.',
  } = props

  const [showConfirmationModal, setShowConfirmationModal] = useState<boolean>()

  return (
    <>
      <div
        onClick={(e) => {
          if (props.confirmationText) {
            setShowConfirmationModal(true)
          } else if (!props.disabled) {
            props.onClick(e)
          }
        }}
        className={
          'h-6 w-6 items-center rounded-md p-[2px]' +
          (props.disabled
            ? ' text-gray-400'
            : ' cursor-pointer hover:bg-gray-400 hover:text-white')
        }
        title={tooltip}
      >
        {props.icon}
      </div>
      {props.confirmationText && showConfirmationModal && (
        <Modal
          isOpen
          onClose={() => setShowConfirmationModal(false)}
          width="w-1/3"
        >
          <div className="flex flex-col gap-2">
            <div>{props.confirmationText}</div>
            <div className="ml-auto flex gap-2">
              <button
                onClick={() => setShowConfirmationModal(false)}
                className={negativeButtonStyle}
              >
                Cancel
              </button>
              <button
                onClick={(e) => {
                  props.onClick(e)
                  setShowConfirmationModal(false)
                }}
                className={buttonStyle}
              >
                Confirm
              </button>
            </div>
          </div>
        </Modal>
      )}
    </>
  )
}
