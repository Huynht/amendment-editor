import { FileEntry, readBinaryFile } from '@tauri-apps/api/fs'
import { useEffect, useMemo, useState } from 'react'
import { Document, Page } from 'react-pdf/dist/esm/entry.vite'
import 'react-pdf/dist/esm/Page/AnnotationLayer.css'
import 'react-pdf/dist/esm/Page/TextLayer.css'

type Props = { file: FileEntry }

function PdfViewer(props: Props) {
  const [numPages, setNumPages] = useState<number>()

  function onDocumentLoadSuccess(input: any) {
    setNumPages(input.numPages)
  }

  const [binary, setBinary] = useState<Uint8Array>()
  useEffect(() => {
    readBinaryFile(props.file.path).then((res) => {
      setBinary(res)
    })
  }, [props.file])

  const fileBla = useMemo(() => {
    if (binary) {
      return new File([binary.buffer], 'bla')
    }
  }, [binary])

  return (
    <>
      <div>
        {fileBla && (
          <div>
            <Document
              file={fileBla}
              onLoadSuccess={(pdf) => onDocumentLoadSuccess(pdf)}
              onLoadError={(e) => console.error(e)}
            >
              {Array.from(new Array(numPages), (el, index) => (
                <Page
                  key={`page_${index + 1}`}
                  pageNumber={index + 1}
                  width={900}
                />
              ))}
            </Document>
          </div>
        )}
      </div>
    </>
  )
}

export default PdfViewer
