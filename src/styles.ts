export const buttonStyle: string =
  'inline-flex items-center px-3 py-2 border border-transparent shadow-sm text-sm leading-4 font-medium text-grey-900 rounded-md bg-blue-300 hover:bg-blue-400 disabled:opacity-50 cursor-pointer'
export const outlineButtonStyle =
  'inline-flex items-center px-3 py-2 border shadow-sm text-sm leading-4 font-medium rounded-md hover:bg-gray-100 disabled:opacity-50 border-blue-300 cursor-pointer'
export const negativeButtonStyle: string =
  outlineButtonStyle + ' border-red-500 text-red-500'
