import { AmendingInstruction } from 'legal-consolidator'

const lineCountMap = new Map<
  | 'DELETE'
  | 'INSERT'
  | 'RENUMBER'
  | 'REPLACE'
  | 'REPLACE_PARTLY'
  | 'REPLACE_KEYWORD'
  | 'RENUMBER_MULTIPLE',
  number
>([
  ['DELETE', 6],
  ['INSERT', 7],
  ['RENUMBER', 9],
  ['RENUMBER_MULTIPLE', 19],
  ['REPLACE', 7],
  ['REPLACE_PARTLY', 8],
  ['REPLACE_KEYWORD', 9],
])

export function getInstructionIndexFromLineNumber(
  instructions: AmendingInstruction[],
  lineNumber: number
) {
  let output: number | undefined = undefined
  let currentCount = 1

  instructions.forEach((instruction, i) => {
    currentCount += lineCountMap.get(instruction.type)!
    if (instruction.type === 'REPLACE_PARTLY') {
      currentCount += instruction.newContent.length
    }

    if (output === undefined && currentCount >= lineNumber) {
      output = i
    }
  })

  return output
}
