import { AmendingInstruction } from 'legal-consolidator'

export function instructionsToJson(instructions: AmendingInstruction[]) {
  return JSON.stringify(instructions, [
    'type',
    'value',
    'newContent',
    'targetChapterKey',
    'newChapterKey',
    'formerKeys',
    'newKeys',
    'from',
    'to',
    'keyword',
    'replacement',
    'recursive',
  ])
}

export const amendingInstructionsSchema = {
  uri: 'huynht.instruction-schema',
  fileMatch: ['*'],
  schema: {
    $schema: 'http://json-schema.org/draft-07/schema#',
    type: 'array',
    items: {
      anyOf: [
        { $ref: '#/definitions/delete-instruction' },
        { $ref: '#/definitions/insert-instruction' },
        { $ref: '#/definitions/renumber-instruction' },
        { $ref: '#/definitions/renumber-multiple-instruction' },
        { $ref: '#/definitions/replace-instruction' },
        { $ref: '#/definitions/replace-partly-instruction' },
        { $ref: '#/definitions/replace-keyword-instruction' },
      ],
    },
    definitions: {
      'delete-instruction': {
        type: 'object',
        properties: {
          type: { enum: ['DELETE'] },
          targetChapterKey: {
            value: { type: 'string' },
          },
        },
        required: ['type', 'targetChapterKey'],
        additionalProperties: false,
      },
      'insert-instruction': {
        type: 'object',
        properties: {
          type: { enum: ['INSERT'] },
          targetChapterKey: {
            value: { type: 'string' },
          },
          newContent: {
            type: 'string',
          },
        },
        required: ['type', 'targetChapterKey', 'newContent'],
        additionalProperties: false,
      },
      'renumber-instruction': {
        type: 'object',
        properties: {
          type: { enum: ['RENUMBER'] },
          targetChapterKey: {
            value: { type: 'string' },
          },
          newChapterKey: {
            value: { type: 'string' },
          },
        },
        required: ['type', 'targetChapterKey', 'newChapterKey'],
        additionalProperties: false,
      },
      'renumber-multiple-instruction': {
        type: 'object',
        properties: {
          type: { enum: ['RENUMBER_MULTIPLE'] },
          formerKeys: {
            from: { value: { type: 'string' } },
            to: { value: { type: 'string' } },
          },
          newKeys: {
            from: { value: { type: 'string' } },
            to: { value: { type: 'string' } },
          },
        },
        required: ['type', 'newKeys', 'formerKeys'],
        additionalProperties: false,
      },
      'replace-instruction': {
        type: 'object',
        properties: {
          type: { enum: ['REPLACE'] },
          targetChapterKey: {
            value: { type: 'string' },
          },
          newContent: {
            value: { type: 'string' },
          },
        },
        required: ['type', 'targetChapterKey', 'newContent'],
        additionalProperties: false,
      },
      'replace-partly-instruction': {
        type: 'object',
        properties: {
          type: { enum: ['REPLACE_PARTLY'] },
          targetChapterKey: {
            value: { type: 'string' },
          },
          newContent: {
            anyOf: [{ value: 'string' }],
          },
        },
        required: ['type', 'targetChapterKey', 'newContent'],
        additionalProperties: false,
      },
      'replace-keyword-instruction': {
        type: 'object',
        properties: {
          type: { enum: ['REPLACE_KEYWORD'] },
          targetChapterKey: {
            value: { type: 'string' },
          },
          keyword: {
            type: 'string',
          },
          replacement: {
            type: 'string',
          },
          recursive: {
            type: 'boolean',
          },
        },
        required: [
          'type',
          'targetChapterKey',
          'keyword',
          'replacement',
          'recursive',
        ],
        additionalProperties: false,
      },
    },
  },
}
