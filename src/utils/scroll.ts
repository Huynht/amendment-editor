import { ChapterKey } from 'legal-consolidator'

export function scrollToChapter(chapterKey: ChapterKey, delayInMs?: number) {
  if (chapterKey.value === 'INSERT_CHAPTER_KEY') return

  const chapterHtmlElement = document.querySelector(
    `[chapter-key='${chapterKey.value}']`
  )
  const scrollableParent = document.getElementById(
    'scrollable-document-container'
  )
  if (!scrollableParent) {
    console.error('container with id #document-container not found')
    return
  }
  if (!chapterHtmlElement) {
    console.error(
      'no element with chapterKey ' + chapterKey.value + ' was found'
    )
    return
  }

  setTimeout(() => {
    scrollableParent.scrollTo({
      top: (chapterHtmlElement as any).offsetTop - 200,
      behavior: 'smooth',
    })
  }, delayInMs)
}
