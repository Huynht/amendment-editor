import { FileEntry, readTextFile } from '@tauri-apps/api/fs'
import { useAtom } from 'jotai'
import { AmendingInstruction, ConsolidationEngine } from 'legal-consolidator'
import { useEffect, useState } from 'react'
import AmendingDocument from '../components/AmendingDocumentViewer'
import AmendmentInput from '../components/AmendmendInput'
import DocumentPicker from '../components/DocumentPicker'
import HtmlPreview from '../components/HtmlPreview/HtmlPreview'
import { currentInputAtom } from '../store'

export default function Consolidate() {
  const [selectedBaseDoc, setSelectedBaseDoc] = useState<
    FileEntry | undefined
  >()
  const [selectedAmendingDoc, setSelectedAmendingDoc] = useState<
    FileEntry | undefined
  >()
  const [consolidationEngine, setConsolidationEngine] = useState(
    () => new ConsolidationEngine()
  )

  const [currentInstructions, setCurrentInstructions] = useState<
    AmendingInstruction[] | undefined
  >([])

  const [_, setCurrentInput] = useAtom(currentInputAtom)

  const [fileViewerRerenderCounter, setFileViewerRerenderCounter] = useState(0)

  return (
    <div className="flex h-full flex-col">
      <div className="flex h-full w-full">
        {/* FILEVIEWER */}
        <div className="w-1/5 p-2">
          <DocumentPicker
            rerenderCounter={fileViewerRerenderCounter}
            onBaseDocSelected={(baseDoc) => {
              setSelectedBaseDoc(baseDoc)
              setCurrentInstructions(undefined)
            }}
            onAmendingDocSelected={(amendingDoc) => {
              setSelectedAmendingDoc(amendingDoc)
              setCurrentInstructions(undefined)
            }}
            onAmendingInstructionsSelected={async (amendingInstructions) => {
              const contents = await readTextFile(amendingInstructions.path)
              setCurrentInput(contents)
            }}
          />
        </div>

        {/* AMENDMENT INPUT */}
        <div className="flex-flex-col h-full w-2/5 gap-2 p-2">
          <div className="h-1/2">
            <div className="text-xl font-bold">
              {selectedAmendingDoc?.path.split('/').slice(-4).join('/')}
            </div>
            <AmendmentInput
              selectedFile={selectedBaseDoc}
              selectedAmendingDoc={selectedAmendingDoc}
              onApply={(instructions) => {
                setCurrentInstructions(instructions)
              }}
              onWriteFile={() => {
                setFileViewerRerenderCounter(fileViewerRerenderCounter + 1)
              }}
            />
          </div>
          <div className="h-1/2">
            <AmendingDocument
              key={selectedAmendingDoc?.path}
              selectedFile={selectedAmendingDoc}
            />
          </div>
        </div>

        {/* PREVIEW */}
        <div className="h-full w-2/5 p-2">
          {selectedBaseDoc && (
            <HtmlPreview
              consolidationEngine={consolidationEngine}
              baseDoc={selectedBaseDoc}
              amendingDoc={selectedAmendingDoc}
              instructions={currentInstructions}
              onWriteFile={() => {
                setFileViewerRerenderCounter(fileViewerRerenderCounter + 1)
              }}
            />
          )}
        </div>
      </div>
    </div>
  )
}
