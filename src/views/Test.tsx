import { platform } from '@tauri-apps/api/os'
import { useEffect, useState } from 'react'
import '../../node_modules/react-keyed-file-browser/dist/react-keyed-file-browser.css'
import FileViewer from '../components/FileViewer/FileViewer'

export default function Test() {
  const [basePath, setBasePath] = useState(
    '/home/an/Dropbox/Studium/aktuell/MA/example_base_html'
  )

  const [os, setOs] = useState('')

  return (
    <>
      <button onClick={() => platform().then((res) => setOs(res))}>
        check OS
      </button>
      {os && os}
      <div
        id="1234"
        tabIndex={0}
        onKeyDown={(e) => {
          console.log(e.code)
          console.log(e.ctrlKey)
        }}
        onClick={(e) => {
          console.log(e)
        }}
        className="h-full border-2 bg-red-200"
      >
        test123
      </div>
    </>
  )
}
